import { useReducer } from "react";

const myRemove = () => {
  console.log("remove");
};

const reducer = (state, action) => {
  const day = new Date();
  const today = `${day.getDate()}-${day.getMonth() + 1}-${day.getFullYear()}`;
  let task;

  if (state)
    task = [
      ...state,
      {
        id: ++action,
        name: "zadanie_" + action,
        date: today,
        done: false,
        remove: () => myRemove(),
      },
    ];
  else
    task = [
      {
        id: ++action,
        name: "zadanie_" + action,
        date: today,
        done: false,
        remove: () => myRemove(),
      },
    ];

  return task;
};

const useCreateList = () => {
  const [state, changeState] = useReducer(reducer, []);

  return { state, changeState };
};

export default useCreateList;
