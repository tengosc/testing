import React, { useEffect, useState } from "react";
import useCreateList from "./customHook";
// import ListItem from "./listItems";

const InterfaceAPI = () => {
  const [passVal, setpassVal] = useState(null);
  const [count, setCount] = useState(0);
  const myValue = useCreateList();

  useEffect(() => {
    handleClick();
  }, []);

  useEffect(() => {}, []);

  const handleClick = () => {
    myValue.changeState(count);
    setpassVal(myValue.state);
    setCount(count + 1);
  };

  const handleChange = (e) => {
    e.target.parentNode.parentNode.classList.toggle("done");
  };

  return (
    <div>
      <div className="col-12 flex__wrapper">
        <p className="col-2">ID </p>
        <p className="col-3">Nazwa</p>
        <p className="col-3">Wykonać do</p>
        <p className="col-2">Wykonane?</p>
        <p className="col-2">usuń</p>
      </div>
      {passVal &&
        passVal.map((el, index) => {
          return (
            <div key={index} className="col-12 flex__wrapper">
              <p className="col-2">{el.id}</p>
              <p className="col-3">{el.name}</p>
              <p className="col-3">{el.date}</p>
              <p className="col-2">
                <input
                  type="checkbox"
                  onChange={(e) => {
                    el.done = !el.done;
                    return handleChange(e);
                  }}
                />
              </p>
              <button onClick={() => el.remove()} className="col-2">
                x
              </button>
            </div>
          );
        })}
      <button onClick={() => handleClick()}>dodaj</button>
    </div>
  );
};

export default InterfaceAPI;
